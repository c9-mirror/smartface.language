define(function(require, exports, module) {
    main.consumes = ["fs"];
    return main;

    function main(options, imports, register) {
        var fs = imports.fs,
            staticPrefix = options.staticPrefix;
        register(null, {});

        var scopeAnalyzer = require('plugins/c9.ide.language.javascript/scope_analyzer');
debugger;
        var GLOBALS = {
            // Literals
            "true": true,
            "false": true,
            "undefined": true,
            "null": true,
            "arguments": true,
            "Infinity": true,
            "continue": true,
            "return": true,
            "else": true,
            alert: true,
            global: true,
            Array: true,
            Boolean: true,
            Date: true,
            decodeURI: true,
            decodeURIComponent: true,
            encodeURI: true,
            encodeURIComponent: true,
            Error: true,
            'eval': true,
            Function: true,
            hasOwnProperty: true,
            isFinite: true,
            isNaN: true,
            JSON: true,
            Math: true,
            Number: true,
            Object: true,
            parseInt: true,
            parseFloat: true,
            RangeError: true,
            ReferenceError: true,
            RegExp: true,
            String: true,
            SyntaxError: true,
            TypeError: true,
            URIError: true,
            // non-standard
            escape: true,
            unescape: true,
            //Smartface
            "atob": true,
            "btoa": true,
            "log": true,
            "alert": true,
            "pick": true,
            "setTimeout": true,
            "clearTimeout": true,
            "setInterval": true,
            "clearInterval": true,
            "Cryptology": true,
            "Data": true,
            "Device": true,
            "Google": true,
            "KOBIL": true,
            "SES": true,
            "SMF": true,
            "Social": true,
            "Application": true,
            "Attr": true,
            "BLOB": true,
            "Comment": true,
            "Dialogs": true,
            "Document": true,
            "DOMParser": true,
            "Element": true,
            "KOFAX": true,
            "Math": true,
            "Menus": true,
            "Node": true,
            "NodeIterator": true,
            "NodeList": true,
            "Notifications": true,
            "Pages": true,
            "Range": true,
            "Text": true,
            "TreeWalker": true,
            "VASCO": true,
            "XMLDocument": true,
            "XMLSerializer": true,
            "XPathNSResolver": true,
            "XPathResult": true,
            "load": true,
            "include": true
        };
        
        scopeAnalyzer.GLOBALS = GLOBALS;
        /*var globals = {
            "true": true,
            "false": true,
            "undefined": true,
            "null": true,
            "arguments": true,
            "Infinity": true,
            "continue": true,
            "return": true,
            "else": true,
            "global": true
        };
        
        generateGlobalsFromDefs([staticPrefix + "/SMF.json", "lib/tern/defs/ecma5.json"], globals, function(result) {
            debugger;
            scopeAnalyzer.GLOBALS = result;
        });
        
        function generateGlobalsFromDefs(defPaths, target, callBack) {
            debugger;
            var path = defPaths.shift();

            fs.readFile(path, 'utf-8', function(err, data) {
                debugger;
                if (err) throw err;
                var propertyName, result,
                    defObject;
                defObject = JSON.parse(defObject);
                for (propertyName in defObject) {
                    if (propertyName[0] !== '!') {
                        defObject[propertyName] = true;
                    }
                    result = mix(target, defObject);
                    if (defPaths.length === 0) {
                        callBack(target);
                    } else {
                        generateGlobalsFromDefs(defPaths, result, callBack);
                    }
                }
            });
        }*/

    }

    function mix() {
        var arg, prop, child = {};
        for (arg = 0; arg < arguments.length; arg += 1) {
            if (!arguments[arg]) {
                continue;
            }
            for (prop in arguments[arg]) {
                if (arguments[arg].hasOwnProperty(prop)) {
                    child[prop] = arguments[arg][prop];
                }
            }
        }
        return child;
    }
});
