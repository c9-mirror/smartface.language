(function (mod) {
    if (typeof exports == "object" && typeof module == "object") // CommonJS
        return mod(require("tern/lib/infer"), require("tern/lib/tern"));
    if (typeof define == "function" && define.amd) // AMD
        return define(["tern/lib/infer", "tern/lib/tern"], mod);
    mod(tern, tern);
})(function (infer, tern) {
    "use strict";
    tern.registerPlugin("smartface", function (server, options) {
        extendTernServerWithRemoteSources(server);
        server.on("beforeLoad", function (file) {
            var found = false;
            var srv = server.server || server;
            var origin;
            var i;
            var fileInfoName;
            var fileInfo;
            for (fileInfoName in srv.remoteSources) {
                fileInfo = srv.remoteSources[fileInfoName];
                for (i = 0; i < fileInfo.origins.length; i++) {
                    origin = fileInfo.origins[i];
                    if (origin.name === file.name) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    found = false;
                    if (file.text.substring(origin.start, origin.end) !== fileInfoName) {
                        fileInfo.origins.splice(i, 1);
                        if (fileInfo.origins.length === 0) {
                            server.delFile(fileInfoName);
                            delete srv.remoteSources[fileInfoName];
                        }
                    }
                    
                }
            }
        });

        return {
            defs: {
                "!name": "smartfaceLoadInc",
                "load": {
                    "!type": "fn(source: string) -> !custom:smartfaceLoad",
                    "!url": "http://http://developer.smartface.io/hc/en-us/articles/202131678L", // You'll want to update
                    "!doc": "evals given source every time into context"
                },
                "include": {
                    "!type": "fn(source: string) -> !custom:smartfaceLoad",
                    "!url": "http://http://developer.smartface.io/hc/en-us/articles/202131678",
                    "!doc": "evals given source once into context"
                }
            }
        };
    });/**/

    // When load/include is called with a string argument, call addFile
    // with that string. The server will pass it on to its getFile
    // callback, which should be set up to be able to handle URL paths.
    infer.registerFunction("smartfaceLoad", function (_self, _args, argNodes) {
        var arg = argNodes && argNodes[0];
        if (arg && arg.type == "Literal" && typeof arg.value == "string" &&
            /^['"].*['"]$/.test(arg.raw)) { // Test whether string was fully typed
            var cx = infer.cx();
            var server = cx.parent;
            //server.addFile(arg.value);
            var fileInfo = {
                name: arg.value,
                origins: [new fileOrigin(cx.curOrigin, arg.start +1, arg.end -1)]
            };
            addRemoteSource(server, fileInfo);

        }
        return infer.ANull;
    });

    function extendTernServerWithRemoteSources(server) {
        var srv = server.server || server;
        if (!srv.remoteSources) {
            srv.remoteSources = {};
        }
    }

    function addRemoteSource(server, fileInfo) {
        var found = false;
        if (!server.remoteSources[fileInfo.name]) {
            server.remoteSources[fileInfo.name] = fileInfo;
        } else {
            var existing = server.remoteSources[fileInfo.name];

            for (var i = 0; i < existing.origins.length; i++) {
                if (fileInfo.origins[0].equals(existing.origins[i])) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                existing.origins.push(fileInfo.origins[0]);
            }
        }
        if (!found) {
            server.addFile(fileInfo.name);
        }
    }
    function applyFileRefChange(server, fileInfo) {
    }

    function fileOrigin(name, start, end) {
        this.start = start;
        this.name = name;
        this.end = end;
    }
    fileOrigin.prototype.equals = function (other) {
        return this.name === other.name && this.start === other.start && this.end === other.end;
    }
});


