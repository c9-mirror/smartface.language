define(function(require, exports, module) {
	main.consumes = ["language.tern"];
	return main;

	function main(options, imports, register) {
		 var ternModule = imports["language.tern"];
		register(null, {});
		
		// 1) Change server options - will increase accuracy with cost of performance
		ternModule.setTernServerOptions({
			reuseInstances: false
		});
		
		// 2) Change request options - will reduce list of completion items to exact relevant list
		ternModule.setTernRequestOptions({
			completions: {
				guess: false
			}
		});
	}
});
